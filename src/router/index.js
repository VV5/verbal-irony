import Vue from 'vue'
import Router from 'vue-router'
import Introduction from '@/components/Introduction'
import Quiz from '@/components/Quiz'
import PostQuiz from '@/components/PostQuiz'
import Comic from '@/components/Comic'
import Instruction from '@/components/Instruction'
import Activity from '@/components/Activity'
import Speech from '@/components/Speech'
import Reflection from '@/components/Reflection'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction',
        header: 'What is Verbal Irony?',
        hasVideo: true
      }
    },
    {
      path: '/quiz',
      name: 'Quiz',
      component: Quiz,
      meta: {
        title: 'Quiz',
        header: 'Quiz',
        hasVideo: false
      }
    },
    {
      path: '/post-quiz',
      name: 'PostQuiz',
      component: PostQuiz,
      meta: {
        title: 'Post Quiz',
        header: 'Viewing: Verbal Irony as Rhetorical Device',
        hasVideo: true
      }
    },
    {
      path: '/comic',
      name: 'Comic',
      component: Comic,
      meta: {
        title: 'Comic',
        header: 'Julius Caesar Comic Strip',
        hasVideo: false
      }
    },
    {
      path: '/instruction',
      name: 'Instruction',
      component: Instruction,
      meta: {
        title: 'Instruction',
        header: 'Read Mark Antony\'s Speech',
        hasVideo: false
      }
    },
    {
      path: '/speech',
      name: 'Speech',
      component: Speech,
      meta: {
        title: 'Speech',
        header: 'View Mark Antony\'s Speech',
        hasVideo: true
      }
    },
    {
      path: '/activity',
      name: 'Activity',
      component: Activity,
      meta: {
        title: 'Activity',
        header: 'Quiz: Spot Verbal Irony!',
        hasVideo: false
      }
    },
    {
      path: '/reflection',
      name: 'Reflection',
      component: Reflection,
      meta: {
        title: 'Reflection',
        header: 'Reflection',
        hasVideo: true
      }
    }
  ]
})
