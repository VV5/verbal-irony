import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    quizzes: [
      {
        id: 1,
        choices: [
          {
            id: 1,
            title: 'Incorrect',
            option: 'A',
            feedback: 'Are you sure? Verbal Irony occurs when a speaker means the opposite of what he/she says on the surface. Given that the girl had won a trophy in school, do you think her parents were sincere about praising her? Try again!',
            selectedWrong: false
          },
          {
            id: 2,
            title: 'Correct',
            option: 'B',
            feedback: 'Well done! The parents were obviously unhappy that the girl had scored an F on her report card. Thus, they were not sincere, when they told her that she did a "good job" on the exams. There is verbal irony because the parents meant the opposite of what they said on the surface.',
            correct: true
          }
        ]
      },
      {
        id: 2,
        choices: [
          {
            id: 1,
            title: 'Correct',
            option: 'A',
            feedback: 'Well done! The speaker had just been hit on the face by the other ballerina in the room. Thus, she could not have been sincere in describing the other ballerina as being “graceful.” Thus, there is verbal irony as she meant the opposite of what she said on the surface.',
            correct: true
          },
          {
            id: 2,
            title: 'Incorrect',
            option: 'B',
            feedback: 'Are you sure? Verbal Irony occurs when the speaker means the opposite of what he/she says on the surface. In this image, do you think the girl was sincere in her remarks to the other ballerina who had just kicked her? Try again!',
            selectedWrong: false
          }
        ]
      }
    ],
    comicStrips: [
      { id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }, { id: 5 }, { id: 6 }
    ],
    speeches: [
      { id: 1 }, { id: 2 }
    ],
    ironies: [
      {
        id: 1,
        text: 'Friends, Romans, countrymen, lend me your ears;',
        selected: false,
        correct: false,
        feedback: 'In the 1st line, Mark Antony is simply calling out to the crowd to pay attention and listen to what he has to say.'
      },
      {
        id: 2,
        text: 'I come to bury Caesar, not to praise him.',
        selected: false,
        correct: true,
        feedback: '<p>Well done!</p>' +
        '<p>Here, Mark Antony says that his purpose for speaking at Caesar\'s funeral is merely to lay the dead Caesar to rest, rather than to pay tribute to him. This is ironic because Mark Antony does subtly remind the crowd of Caesar\'s good qualities in the rest of the speech.</p>'
      },
      {
        id: 3,
        text: 'The evil that men do lives after them;\n The good is often interred with their bones;\n so let it be with Caesar.',
        selected: false,
        correct: true,
        feedback: '<p>Good Job!</p>' +
        '<p>In the first 2 lines, Mark Antony is saying that the evil that men do is usually remembered after their deaths, while the good is often forgotten, and it should be the same with Caesar. Note that the first two lines are not ironic in themselves as it a fact that many notorious people in history are remembered for their evil deeds, which tend to overshadow any good they have done in their lives.</p>' +
        '<p>However, Mark Antony\'s last line "So let it be with Caesar" makes the whole statement ironic as he does not intend to highlight Caesar\'s flaws, but remind the crowd of his good qualities in the rest of his speech.</p>'
      },
      {
        id: 4,
        text: 'The noble Brutus hath told you Caesar was ambitious;',
        selected: false,
        correct: true,
        feedback: '<p>Excellent!</p>' +
        '<p>Here, Mark Antony calls Brutus "noble", which he does not mean, as he feels that Brutus\' murder of Caesar was unjust. Thus, verbal irony is employed as he means the opposite of what he says.</p>'
      },
      {
        id: 5,
        text: 'If it were so, it was a grievous fault,\n And grievously hath Caesar answer’d it.',
        selected: false,
        correct: false,
        feedback: 'Mark Antony is stating a fact here: If Caesar had been an ambitious tyrant, it would have been a serious fault, and he would already have paid for it gravely with his life.'
      },
      {
        id: 6,
        text: 'Here, under leave of Brutus and the rest',
        selected: false,
        correct: false,
        feedback: 'Here, Mark Antony is saying that it was only with the permission of Brutus and the rest that he has come to speak at Caesar\'s funeral. This is a fact as Brutus allowed Mark Antony to have an audience with the crowd.'
      },
      {
        id: 7,
        text: 'For Brutus is an honorable man;\n So are they all, all honorable men',
        selected: false,
        correct: true,
        feedback: '<p>Well done!</p>' +
        '<p>Here, Mark Antony calls Brutus and the conspirators "honourable men", which he does not mean, as he feels that their murder of Caesar was unjust. Thus, verbal irony is employed as he means the opposite of what he says.</p>'
      },
      {
        id: 8,
        text: 'Come I to speak in Caesar’s funeral.\n He was my friend, faithful and just to me:',
        selected: false,
        correct: false,
        feedback: 'Here, Mark Antony calls Caesar a friend who was loyal to him, which is true in the context of this play.'
      },
      {
        id: 9,
        text: 'But Brutus says he was ambitious;\n And Brutus is an honorable man.',
        selected: false,
        correct: true,
        feedback: '<p>Excellent!</p>' +
        '<p>Here, Mark Antony calls Brutus an "honourable man", which he does not mean, as he feels that his murder of Caesar was unjust. This, verbal irony is employed as he means the opposite of what he says. Do note that Antony repeats this line from before, heightening the verbal irony.</p>'
      },
      {
        id: 10,
        text: 'He hath brought many captives home to Rome\n Whose ransoms did the general coffers fill:\n Did this in Caesar seem ambitious?\n When that the poor have cried, Caesar hath wept:\n Ambition should be made of sterner stuff:',
        selected: false,
        correct: false,
        feedback: 'In these lines, Mark Antony lists Caesar\'s good qualities. He states that Caesar brought many captives home to Rome, and their ransoms brought wealth to the city. When the poor suffered and wept, Caesar empathised with them too, feeling their pain as if it was his own. Antony states that an ambitious man would have been firmer and stronger, not compassionate.'
      },
      {
        id: 11,
        text: 'Yet Brutus says he was ambitious;\n And Brutus is an honorable man.',
        selected: false,
        correct: true,
        feedback: '<p>Good Job!</p>' +
        '<p>Here, Mark Antony calls Brutus an "honourable man", which he does not mean, as he feels that his murder of Caesar was unjust. Thus, verbal irony is employed as he means the opposite of what he says. Do note that this is the second time Antony repeats this line, heightening the verbal irony.</p>'
      },
      {
        id: 12,
        text: 'You all did see that on Lupercal\n I thrice presented him a kingly crown,\n Which he did thrice refuse: was this ambition?',
        selected: false,
        correct: false,
        feedback: 'In this line, Mark Antony questions the crowd if they still feel that Caesar is ambitious when he refused the crown three times after Antony offered it during a feast.'
      },
      {
        id: 13,
        text: 'Yet Brutus says he was ambitious;\n And, sure, he is an honorable man.\n I speak not to disprove what Brutus spoke,\n But here I am to speak what I do know.',
        selected: false,
        correct: true,
        feedback: '<p>Good Job!</p>' +
        '<p>Here, Mark Antony calls Brutus an "honourable man", which he does not mean, as he feels that his murder of Caesar was unjust. Do note that this is the third time Antony repeats this line, heightening the verbal irony.</p>' +
        '<p>Similarly, he claims that he is not here to "disprove what Brutus spoke" about Caesar, which is untrue because his intention is to undermine Brutus and turn the crowd against him.</p>' +
        '<p>Thus, verbal irony is employed as he means the opposite of what he says.</p>'
      }
    ],
    selectedChoice: {},
    showSidebar: false,
    isLandscape: true
  },
  mutations: {
    isLandscape (state, status) {
      state.isLandscape = status
    },
    setSelectedChoice (state, choice) {
      state.selectedChoice = choice
    },
    setSelectedWrong (state, payload) {
      const quizIndex = state.quizzes.indexOf(payload.quiz)
      const choiceIndex = state.quizzes[quizIndex].choices.indexOf(payload.choice)

      state.quizzes[quizIndex].choices[choiceIndex].selectedWrong = true
    },
    toggleSidebar (state) {
      state.showSidebar = !state.showSidebar
    },
    setIronySelected (state, choice) {
      const index = state.ironies.indexOf(choice)

      state.ironies[index].selected = true
    }
  },
  getters: {
    animatedVideos (state) {
      return state.quizzes.map(quiz => quiz.id)
    },
    speeches (state) {
      return state.speeches.map(speech => speech.id)
    }
  }
})
