# Changelog

## v2.1.5
- Fixed audio for Activity

## v2.1.4
- Updated feedback audio for Activity
- Updated `@vue/test-utils` and fix tests

## v2.1.3
- Added a function to navigate through the comic strips using keyboard keys
- Added transitions to components

## v2.1.2
- Fixed responsiveness for components
- Added package for Bulma tooltips
- Updated comic assets

## v2.1.1
- Fixed comic scroll not rendering correctly
- Fixed UI/UX styling
- Upgraded `vue-loader` to v15

## v2.1.0
- Updated video assets
- Upgraded dependencies to be compatible with `node v10`
- Fixed opening of modal despite image is already disabled
- Fixed UI for Instructions component
- Added a feedback for quiz for correct and wrong responses
- Reverted from `MiniCssExtractPlugin` to `ExtractTextPlugin`
- Added optimisation for Webpack 4

## v2.0.1
- Removed unused Font Awesome libraries
- Updated comic to use `.png` instead of `.jpg`
- Added toggleable Sidebar
- Added "restart" button to end of the page
- Fixed comic modal not behaving correctly to clicks

## v2.0.0
### Breaking changes
- Upgrade to Webpack v4

## 1.0.0
### Initial release
- Updated comic assets with colour
- Massive styling added to whole site
- Added animated videos

## 0.3.0
- Added audio assets
- Added styling to Activity page

## 0.2.1
- Added a button to skip to Speech in comic strip page
- Updated comic strip
- Fixed videos causing page to "scroll"
- Fixed video having a non-black background colour

## 0.2.0
- Added a button on Comic Strip modal
- Added Mark Anthony speech
- Fixed and improved styling for animated video component
- Fixed responsiveness in Comic Strip

## 0.1.0
Internal initial release
