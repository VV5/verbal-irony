# Verbal Irony
[![pipeline status](https://gitlab.com/VV5/verbal-irony/badges/master/pipeline.svg)](https://gitlab.com/VV5/verbal-irony/commits/master)
[![codecov](https://codecov.io/gl/VV5/verbal-irony/branch/master/graph/badge.svg)](https://codecov.io/gl/VV5/verbal-irony)

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
