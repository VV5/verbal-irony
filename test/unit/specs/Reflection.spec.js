import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Reflection from '@/components/Reflection'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Reflection', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Reflection)
  })

  test('should navigate to the beginning when clicked', () => {
    const $route = {
      path: '/'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Reflection, {
      router, localVue
    })

    wrapper.find('video').trigger('ended')

    wrapper.find('.restart-button').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
