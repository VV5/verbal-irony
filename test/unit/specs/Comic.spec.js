import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Comic from '@/components/Comic'
import ComicModal from '@/components/ComicModal'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Comic', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {
      comicStrips: [
        { id: 1 }, { id: 2 }, { id: 3 }
      ]
    }

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(Comic, {
      attachToDocument: true,
      store,
      localVue
    })
  })

  test('should close the comic modal when esc key is pressed', () => {
    wrapper.setData({ isModalActive: true })

    wrapper.trigger('keyup', { key: 'Escape' })

    expect(wrapper.vm.isModalActive).toBe(false)
  })

  test('nothing should happen when keys other than esc is pressed', () => {
    wrapper.setData({ isModalActive: true })

    wrapper.trigger('keyup')

    expect(wrapper.vm.isModalActive).toBe(true)
  })

  test('should open a modal displaying the respective comic strip', () => {
    wrapper.findAll('.is-comic').at(0).trigger('click')

    expect(wrapper.vm.selectedComic).toBe(1)
    expect(wrapper.vm.isModalActive).toBe(true)
  })

  test('should navigate comic backward if back button is clicked', () => {
    wrapper.findAll('.is-comic').at(1).trigger('click')

    expect(wrapper.vm.selectedComic).toBe(2)

    wrapper.find(ComicModal).vm.$emit('navigateComic', -1)

    expect(wrapper.vm.selectedComic).toBe(1)
  })

  test('should navigate comic forward if next button is clicked', () => {
    wrapper.findAll('.is-comic').at(0).trigger('click')

    expect(wrapper.vm.selectedComic).toBe(1)

    wrapper.find(ComicModal).vm.$emit('navigateComic', 1)

    expect(wrapper.vm.selectedComic).toBe(2)
  })

  test('should navigate comic to the first comic strip when home button is clicked', () => {
    wrapper.findAll('.is-comic').at(2).trigger('click')

    expect(wrapper.vm.selectedComic).toBe(3)

    wrapper.find(ComicModal).vm.$emit('navigateComic', 0)

    expect(wrapper.vm.selectedComic).toBe(1)
  })

  test('should close comic model', () => {
    wrapper.setData({ isModalActive: true })

    wrapper.find(ComicModal).vm.$emit('closeComicModal')

    wrapper.vm.$emit('closeComicModal')

    expect(wrapper.vm.isModalActive).toBe(false)
  })

  test('should navigate correctly when button is clicked', () => {
    const $route = {
      path: '/instruction'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Comic, {
      store, router, localVue
    })

    wrapper.find('.goto-instruction').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
