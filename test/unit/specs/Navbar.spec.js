import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Navbar from '@/components/Navbar'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Navbar', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {
      showSidebar: false
    }

    mutations = {
      toggleSidebar: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    wrapper = shallowMount(Navbar, {
      store, localVue
    })
  })

  test('should toggle sidebar correctly', () => {
    expect(wrapper.vm.showSidebar).toBe(false)

    wrapper.find('.navbar-item').trigger('click')

    expect(mutations.toggleSidebar).toHaveBeenCalled()
  })
})
