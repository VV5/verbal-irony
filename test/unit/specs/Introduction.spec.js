import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import Router from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(Router)

describe('Introduction', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Introduction)
  })

  test('should open a link when image thumbnail is clicked', () => {
    const openLink = sinon.stub(window, 'open')

    wrapper.find('img').trigger('click')

    expect(openLink.called).toBe(true)
  })

  test('should start the game when start button is clicked', () => {
    const $route = {
      path: '/quiz'
    }

    const router = new Router()

    wrapper = shallowMount(Introduction, {
      router, localVue
    })

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
