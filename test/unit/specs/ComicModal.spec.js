import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import ComicModal from '@/components/ComicModal'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('ComicModal', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {
      comicStrips: [
        { id: 1 }, { id: 2 }
      ]
    }

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(ComicModal, {
      attachToDocument: true,
      store,
      localVue,
      propsData: {
        isActive: true,
        selectedComic: 1
      }
    })
  })

  test('should go to the correct comic strip when their respective arrow buttons are pressed', () => {
    wrapper.trigger('keyup', { key: 'ArrowLeft' })
    wrapper.vm.$emit('navigateComic', -1)

    expect(wrapper.emitted().navigateComic).toBeTruthy()

    wrapper.trigger('keyup', { key: 'ArrowRight' })
    wrapper.vm.$emit('navigateComic', 1)

    expect(wrapper.emitted().navigateComic).toBeTruthy()
  })

  test('should not be able to navigate less than the items in comicStrips', () => {
    wrapper.setProps({ selectedComic: 2 })

    wrapper.trigger('keyup', { key: 'ArrowLeft' })

    expect(wrapper.vm.selectedComic).toBe(2)
  })

  test('should not be able to navigate more than the items in comicStrips', () => {
    wrapper.setProps({ selectedComic: wrapper.vm.comicStrips.length })

    wrapper.trigger('keyup', { key: 'ArrowRight' })

    expect(wrapper.vm.selectedComic).toBe(2)
    expect(wrapper.emitted().navigateComic).toBeTruthy()
  })

  test('should not have anything happening when buttons other than left and right arrows are pressed', () => {
    const keyup = wrapper.trigger('keyup')

    expect(keyup).toBeFalsy()
  })

  test('should close modal when modal background is clicked', () => {
    wrapper.find('.modal-background').trigger('click')

    expect(wrapper.vm.$emit('closeModal')).toBeTruthy()
  })

  test('should navigate comic strip in sequence when arrow buttons are clicked', () => {
    wrapper.find('.is-next').trigger('click')

    wrapper.vm.$emit('navigateComic')

    expect(wrapper.emitted().navigateComic).toBeTruthy()
  })

  test('should navigate correctly', () => {
    wrapper = shallowMount(ComicModal, {
      store,
      localVue,
      propsData: {
        isActive: true,
        selectedComic: 2
      }
    })

    expect(wrapper.vm.selectedComic).toBe(2)

    wrapper.find('.is-next').trigger('click')

    wrapper.vm.$emit('gotoInstruction')

    expect(wrapper.emitted().gotoInstruction).toBeTruthy()
  })
})
