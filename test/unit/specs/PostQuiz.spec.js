import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import PostQuiz from '@/components/PostQuiz'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('PostQuiz', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(PostQuiz)
  })

  test('defaults does not show overlay', () => {
    expect(wrapper.vm.videoEnded).toBe(false)
  })

  test('shows overlay with button when video has ended', () => {
    wrapper.setData({
      videoEnded: true
    })

    expect(wrapper.find('button').text()).toBe('Read Comic')
  })

  test('should navigate to correct path', () => {
    const $route = {
      path: '/comic'
    }

    const router = new VueRouter()

    const wrapper = shallowMount(PostQuiz, {
      mocks: {
        $store: {
          getters: {
            animatedVideos: () => []
          }
        }
      },
      router,
      localVue
    })

    wrapper.setData({ videoEnded: true })

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
