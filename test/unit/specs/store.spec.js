import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import store from '@/store'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('store', () => {
  test('should toggle between landscape and portrait mode correctly', () => {
    expect(store.state.isLandscape).toBe(true)

    store.commit('isLandscape', false)

    expect(store.state.isLandscape).toBe(false)
  })

  test('should generate animated videos correctly', () => {
    expect(store.getters.animatedVideos).toEqual([1, 2])
  })

  test('should generate speeches correctly', () => {
    expect(store.getters.speeches).toEqual([1, 2])
  })

  test('update showSidebar when committed', () => {
    store.commit('toggleSidebar')
    expect(store.state.showSidebar).toBe(true)

    store.commit('toggleSidebar')
    expect(store.state.showSidebar).toBe(false)
  })

  test('update selectedChoice', () => {
    store.commit('setSelectedChoice')
    expect(store.state.selectedChoice).not.toBeNull()
  })

  test('update setIronySelected to true', () => {
    const irony = store.state.ironies[0]

    store.commit('setIronySelected', irony)

    expect(store.state.ironies[0].selected).toBe(true)
  })
})
