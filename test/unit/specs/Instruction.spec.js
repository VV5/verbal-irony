import { shallowMount, createLocalVue } from '@vue/test-utils'
import Router from 'vue-router'
import Instruction from '@/components/Instruction'

const localVue = createLocalVue()
localVue.use(Router)

describe('Instruction', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Instruction)
  })

  test('should add words to array when condition is met', () => {
    wrapper.setData({ storyIndex: 1 })

    expect(wrapper.vm.arraySelectedWords.length).toBe(0)

    const word = wrapper.findAll('.highlighted-word').at(0)
    word.trigger('click')

    const inArray = wrapper.vm.arraySelectedWords.some(selectedWord => selectedWord === word.element.dataset.word)

    expect(inArray).toBe(true)

    expect(wrapper.vm.arraySelectedWords.length).toBe(1)
  })

  test('should increase storyIndex when button is clicked', () => {
    expect(wrapper.vm.storyIndex).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.storyIndex).toBe(1)
  })

  test('navigate correctly to speech when button is clicked', () => {
    const $route = {
      path: '/speech'
    }

    const router = new Router()

    wrapper = shallowMount(Instruction, {
      router, localVue
    })

    wrapper.setData({ storyIndex: 2 })

    wrapper.find('.goto-speech').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
