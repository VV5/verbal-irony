import { shallowMount } from '@vue/test-utils'
import ActivityModal from '@/components/ActivityModal'

describe('ActivityModal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ActivityModal)
  })

  test('should close the modal when modal background is clicked', () => {
    wrapper.find('.modal-background').trigger('click')
    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })

  test('should close the modal when button is clicked', () => {
    wrapper.find('.okay-button').trigger('click')
    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })
})
