import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import QuizImage from '@/components/QuizImage'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('QuizImage', () => {
  let wrapper, store, state, mutations, computed

  beforeEach(() => {
    mutations = {
      setSelectedChoice: jest.fn(),
      setSelectedWrong: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    computed = {
      image: () => 'quiz-1-a.jpg'
    }

    wrapper = shallowMount(QuizImage, {
      store,
      localVue,
      propsData: {
        quiz: {},
        choice: {}
      },
      computed
    })
  })

  test('should return the correct image computed from props', () => {
    wrapper.setProps({
      quiz: { id: 1 },
      choice: { option: 'A' }
    })

    expect(wrapper.vm.image).toBe('quiz-1-a.jpg')
  })

  test('', () => {
    wrapper.setProps({
      quiz: { id: 1 },
      choice: { selectedWrong: true }
    })

    wrapper.find('img.is-disabled').trigger('click')

    expect(wrapper.emitted().openModal).toBeFalsy()
  })

  test('should feedback with the wrong answer when the image for the wrong answer is clicked', () => {
    wrapper.setData({ choice: { id: 1 } })

    wrapper.find('img').trigger('click')
    wrapper.vm.$emit('setSelectedWrong')

    expect(wrapper.emitted().setSelectedWrong).toBeTruthy()
  })

  test('should continue to next set', () => {
    wrapper.setData({ choice: { id: 1, correct: true } })

    wrapper.find('img').trigger('click')
    wrapper.vm.$emit('incrementQuizIndex')

    expect(wrapper.emitted().incrementQuizIndex).toBeTruthy()
  })
})
