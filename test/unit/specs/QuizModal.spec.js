import { shallowMount } from '@vue/test-utils'
import QuizModal from '@/components/QuizModal'

describe('QuizModal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(QuizModal, {
      propsData: {
        isActive: true,
        selectedChoice: {
          option: 'A'
        },
        quizIndex: 0
      }
    })
  })

  test('should increment quizIndex when the correct answer is selected', () => {
    wrapper.setProps({
      selectedChoice: {
        option: 'A',
        correct: true
      }
    })

    wrapper.find('.okay-button').trigger('click')

    wrapper.vm.$emit('incrementQuizIndex')

    expect(wrapper.emitted().incrementQuizIndex).toBeTruthy()
  })

  test('should close the modal when the wrong answer is selected', () => {
    wrapper.find('.okay-button').trigger('click')

    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })
})
