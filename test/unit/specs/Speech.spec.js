import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Router from 'vue-router'
import Speech from '@/components/Speech'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Router)

describe('Speech', () => {
  let wrapper, store, state, getters

  beforeEach(() => {
    state = {}

    getters = {
      speeches: jest.fn()
    }

    store = new Vuex.Store({
      state, getters
    })

    wrapper = shallowMount(Speech, {
      store, localVue
    })
  })

  test('should set video ended to true when video has finished playing', () => {
    wrapper.find('video').trigger('ended')

    expect(wrapper.vm.videoEnded).toBe(true)
  })

  test('should navigate correctly if button is clicked', () => {
    const $route = {
      path: '/activity'
    }

    const router = new Router()

    wrapper = shallowMount(Speech, {
      store, router, localVue
    })

    wrapper.setData({ videoEnded: true })

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
