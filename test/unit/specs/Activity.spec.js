import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Activity from '@/components/Activity'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Activity', () => {
  let wrapper, store, state, mutations
  let router

  beforeEach(() => {
    state = {
      ironies: [
        { text: 'Foo', selected: false, correct: true, feedback: 'Foo' },
        { text: 'Bar', selected: false, correct: false, feedback: 'Bar' }
      ]
    }

    mutations = {
      setIronySelected: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    router = new VueRouter()

    wrapper = shallowMount(Activity, {
      attachToDocument: true,
      store,
      router,
      localVue
    })

    window.HTMLMediaElement.prototype.load = () => {}
    window.HTMLMediaElement.prototype.play = () => {}
  })

  test('should close modal when Enter key is pressed', () => {
    expect(wrapper.vm.isActive).toBe(true)

    wrapper.trigger('keyup', { key: 'Enter' })

    expect(wrapper.vm.isActive).toBe(false)
  })

  test('should close modal when Esc key is pressed', () => {
    expect(wrapper.vm.isActive).toBe(true)

    wrapper.trigger('keyup', { key: 'Escape' })

    expect(wrapper.vm.isActive).toBe(false)
  })

  test('should increment correct count when correct option is clicked', () => {
    wrapper.setData({ isActive: false })

    expect(wrapper.vm.correctCount).toBe(0)

    wrapper.findAll('.is-irony').at(0).trigger('click')

    expect(wrapper.vm.correctCount).toBe(1)
  })

  test('should not increment correct count when the wrong option is clicked.', () => {
    wrapper.setData({ isActive: false })

    expect(wrapper.vm.correctCount).toBe(0)

    wrapper.findAll('.is-irony').at(1).trigger('click')

    expect(wrapper.vm.correctCount).toBe(0)
  })

  test('should navigate to reflection when button is clicked', () => {
    const $route = {
      path: '/reflection'
    }

    wrapper = shallowMount(Activity, {
      attachToDocument: true,
      store,
      router,
      localVue,
      computed: {
        hasSelectedChoice: () => true,
        showNextButton: () => true
      }
    })

    wrapper.setData({
      isActive: false
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
