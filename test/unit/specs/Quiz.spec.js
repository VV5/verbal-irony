import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Quiz from '@/components/Quiz'
import QuizImage from '@/components/QuizImage'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Quiz', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {
      quizzes: [
        {
          id: 1,
          choices: [
            { option: 'A' },
            { option: 'B', correct: true }
          ]
        },
        {
          id: 2,
          choices: [
            { option: 'A', correct: true },
            { option: 'B' }
          ]
        }
      ]
    }

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(Quiz, {
      store, localVue
    })
  })

  test('should open modal when an item is clicked', () => {
    wrapper.find(QuizImage).vm.$emit('openModal')

    expect(wrapper.vm.isActive).toBe(true)
  })

  test('should increment quiz index when image is clicked', () => {
    expect(wrapper.vm.quizIndex).toBe(0)

    wrapper.find(QuizImage).vm.$emit('incrementQuizIndex')

    expect(wrapper.vm.quizIndex).toBe(1)
  })

  test('should navigate to Post Quiz if all quizzes are completed', () => {
    const $route = {
      path: '/post-quiz'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Quiz, {
      store, router, localVue
    })

    wrapper.setData({ quizIndex: 1 })

    wrapper.find(QuizImage).vm.$emit('incrementQuizIndex')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
