import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Sidebar from '@/components/Sidebar'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Sidebar', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {}

    mutations = {
      toggleSidebar: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })
  })

  test('', () => {
    const router = new VueRouter()

    wrapper = shallowMount(Sidebar, {
      store, router, localVue
    })

    wrapper.setData({
      routes: [
        {
          path: '/',
          meta: {
            hidden: false
          }
        }
      ]
    })

    wrapper.findAll('li').at(0).trigger('click')

    expect(mutations.toggleSidebar).toHaveBeenCalled()
  })
})
